\subsection{Application Research}
\label{sec:apps}

The project's approach is to design and build a variety of applications on NDN to drive the development and
deployment of the architecture and its supporting modules, to test 
prototype implementations, and to encourage community use, experimentation,
and feedback into the design.
%based on a broad vision for future applications. 
Application-driven development also allows verification and validation of
performance and functional advantages of NDN, such as how routing on names 
	%NDN's embedding of application names in the routing system 
promotes efficient authoring of sophisticated distributed
applications, by reducing complexity, opportunities for error, and time 
and expense of design and deployment.
A few years of designing and developing prototype applications
on NDN has revealed five key areas of application research 
that map to important features of the architecture: (1) namespaces;  
(2) trust models;  (3) in-network storage;  (4) data synchronization;  
(5) rendezvous, discovery, and bootstrapping.
	%for each class of application.  
	%As might be expected for a name-based architecture, the 
	%latter four also necessarily involve namespace design.  
These challenges arise within and across applications.  
Namespace design must also
recognize the interplay between application-specific requirements 
for data distribution and organization of trust-related information, together with those imposed for efficient routing/forwarding.  Similar
challenges exist in name discovery, bootstrapping, and mobility support.  
This commitment to application development paid off early in
the project: it uncovered the unanticipated importance of both a per-node
repository for persistent storage, and synchronization as a general
building block for applications.  A few examples of early applications 
illustrate NDN's benefits and challenges.

\paragraph*{Video Streaming}

One of the first NDN applications was a functional video streaming 
application that demonstrated the practical benefits of NDN-based
media delivery, which inherently supports caching and multicast. 
\emph{NDNVideo}~\cite{videoTR}
streams live and pre-recorded HD video via NDN,
and has been tested and demonstrated over both UDP and Ethernet
transport. In its most recent live demonstration, 1000 clients across 
Amazon Web Services and the NDN testbed consumed video from a single NDNVideo
publisher, with only the ``plain vanilla'' NDN forwarder on intermediate
nodes \cite{CrowleyNEAPresentation}.  The NDNVideo application
does not require direct communication between publisher and consumer,
enabling publisher-independent scalability through NDN's use of in-network
storage.  
%\footnote{See
%Interlude's \emph{Treehouse} nonlinear video delivery platform, used for
%the recent Bob Dylan interactive music video (\url{http://video.bobdylan.com}),
%for an example of such emerging applications in the current Internet.},
Applications that perform on-the-fly assembly of content or 
selection of video sections, i.e., frame-level random access requirements, 
are supported directly through namespace design. 
%@ too much detail
%NDNVideo provides a
%timecode-based keyframe namespace, which can be used by a consumer to
%easily seek to any frame in the video by a known naming convention rather
%than a special protocol.

%, introduced in Section~\ref{sec:transport},
\paragraph*{Real-time Conferencing} 
The \emph{ChronoChat}~\cite{ChronosTR} multi-user text chat application has provided a platform to explore data synchronization techniques that can support a peer-to-peer (i.e., no centralized server) chat service. 
	%(Peer-to-peer communication models are a significant emphasis 
	%of NDN application work, given on the challenges faced in 
	%implementing them on the current Internet.) 
ChronoChat has also motivated experimentation
with non-hierarchical trust models (section~\ref{sec:trust}),
and development of library support for encryption-based access
control.  Combining experience from ChronoChat, NDNVideo, and early
work on an NDN \emph{Audio Conference Tool}~\cite{act-sec}, is 
inspiring development of \emph{ndnrtc}, a 
videoconferencing application incorporating the WebRTC codebase.  
This tool will enable investigation of NDN-specific approaches to 
congestion control, rate adaptation, and playout synchronization 
for real-time communication.  
% too much detail
%The ndnrtc implementation uses  native
%code for media and crypto-related functions (in C++) and browser-based
%code for the user interface and functions such as conference rendezvous
%and setup, using the NDN-JS Javascript library.

\paragraph*{Building Automation Systems} 
Enterprise building automation and management systems (BAS/BMS) are an ideal 
driver for NDN research, since a carefully designed namespace and trust model can support authenticated control of sensors~\cite{NDN-lighting-NOM}.  One of the largest NDN application research efforts thus far has been a collaboration with
UCLA Facilities Management, which operates a network with over 150K
points of sensing and control, and has facilitated both the installation
of dedicated, industry-standard electrical demand monitoring system and
access to data from existing systems for NDN research~\cite{ndn-bms}.
BAS/BMS applications pose different requirements for data naming and
trust than multimedia applications. 
For example, the current NDN-based BMS design publishes data in 
three namespaces:
one for application data access that follows the physical building system
configuration, another for device discovery and bootstrapping, and a trust
management namespace for keys that embodies the institutional roles and
relationships of the principals.
Another challenge is to explore how namespace and storage design
can support data aggregation and mining from many heterogeneous
sensors and other devices. 
%In addition to these high level considerations, the application
%domain also motivates us to explore how to provide an NDN stack and
%modules suitable for a range of devices, from embedded hardware to
%workstation class machines.

%More recently, we have collaborated to explore how NDN can support
%vehicular networking applications through its robust support for
%opportunistic communication
%Vehicular networking
%motivates a number of research challenges in supporting mobility and
%embedded hardware, as well as enabling a variety of service interactions--e.g.,
%behind-the-scenes communications with the car manufacturer and infrastructure
%as well as interactive applications for passengers.  In particular, we
%are using applications within this domain to drive research on naming

\paragraph*{Vehicular Networking} 
Vehicular networking is another domain where the NDN architecture
offers advantages, enabling location-based content retrieval 
and new trust models to support ad-hoc, opportunistic 
communication~\cite{VANET-NDN-NOMEN14}.  
Experimentation with vehicular applications has also led to 
updates to the NDN protocol stack itself, including support for other
media (e.g., 3G/LTE, DSRC/WAVE, WiFi, WiMAX) and network-layer support
for {\em data muling}, where vehicular NDN nodes cache data packets heard
over a broadcast channel that do not have matching pending Interest in
their PIT, in order to later provide them to other vehicles or pass them
to infrastructure.


\paragraph*{Other Applications}  The available NDN software 
platform \cite{codebase}
%\footnote{\url{http://named-data.net/codebase/platform/}} 
has enabled students and others to explore NDN-based distributed file
systems, multi-user games, and network management tools.  
Over the next few years, work will continue on the applications
above, as well as new explorations of climate modeling and mobile 
health environments as drivers of NDN architecture research and development.
%naming, in-network storage, and trust research. 

%\subsubsection{Sync}
%Traditional transport services provide point-to-point data delivery and
%most of today's distributed applications, including peer-to-peer
%applications, heavily rely on centralized servers.


\paragraph*{New architecture component: Sync} 
%Most of today's distributed applications, including peer-to-peer
%applications, heavily rely on centralized servers.  
%After several years trying to build 
As a direct result of trying to build robust, efficient and
truly distributed (i.e., serverless)
peer-to-peer NDN applications, the architecture now supports a 
new building block called \emph{Sync}~\cite{Afanasyev13:CHRONOSYNC}.  
Using NDN's basic Interest-Data exchange communication model, Sync uses
naming conventions to enable multiple parties to synchronize their
datasets.  By exchanging individually computed data digests, each party
learns about new or missing data quickly and reliably, and then
can retrieve data efficiently via NDN's built-in multicast delivery.
